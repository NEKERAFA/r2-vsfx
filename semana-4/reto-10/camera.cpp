#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "camera.h"

Camera::Camera(const glm::vec3& position, const glm::vec3& point_to) :
    position(position),
    front(glm::normalize(point_to - position)),
    up(glm::vec3(0.0f, 1.0f, 0.0f))
{}

Camera::Camera(const glm::vec3& position) :
    position(position),
    front(glm::vec3(0.0f, 0.0f, -1.0f)),
    up(glm::vec3(0.0f, 1.0f, 0.0f))
{}

void Camera::move(const float dFront, const float dRight) {
    position += dFront * front + dRight * glm::normalize(glm::cross(front, up));
}

void Camera::rotation(const float yaw, const float pitch) {
    glm::vec3 direction;
    direction.x = cos(glm::radians(yaw) * cos(glm::radians(pitch)));
    direction.y = sin(glm::radians(pitch));
    direction.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
    front = glm::normalize(direction);
}

glm::mat4 Camera::getTransform() {
    return glm::lookAt(position, position + front, up);
}