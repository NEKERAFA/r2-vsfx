#include <glad/gl.h>

#include "object.h"

Object::Object() : success(false), vbo(0), vao(0), ebo(0) {}

Object::~Object() {
    if (success) {
        glDeleteVertexArrays(1, &vao);
        glDeleteBuffers(1, &vbo);
        glDeleteBuffers(1, &ebo);
    }
}

bool Object::isSuccess() {
    return success;
}