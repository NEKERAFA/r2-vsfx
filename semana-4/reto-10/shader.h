#include <string>

#ifdef _WIN32
#include <SDL.h>
#include <SDL_opengl.h>
#else
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#endif

#include <glm/glm.hpp>

#ifndef GL_SHADER_H
#define GL_SHADER_H

class Shader {
    private:
        GLuint program_id;
        bool success;

    public:
        Shader(const std::string& filename);
        ~Shader();
        GLuint getProgramID() const;
        void useProgram() const;
        bool isSuccess() const;
        GLuint getUniformLocation(const std::string& uniform) const;
        void setUniformMat4(const GLuint uniform, const glm::mat4& mat) const;
};

#endif // GL_SHADER_H
