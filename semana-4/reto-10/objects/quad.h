#include "../object.h"

#ifndef QUAD_OBJ_H
#define QUAD_OBJ_H

class Quad : public Object {
    public:
        // Create quad
        Quad();
        // Draw quad
        void draw() override;
};

#endif // QUAD_OBJ_H