#include <glad/gl.h>

#include "../object.h"

#ifndef QUAD_TEXTURED_OBJ_H
#define QUAD_TEXTURED_OBJ_H

class QuadTextured : public Object {
    private:
        GLuint texture;

    public:
        // Create quad with texture
        QuadTextured(const GLuint& texture);
        // Draw quad with texture
        void draw() override;
};

#endif // QUAD_TEXTURED_OBJ_H