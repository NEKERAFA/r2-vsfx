#include <glad/gl.h>

#include "quad.h"

Quad::Quad() {
    // vertex data as (x, y, z, r, g, b)
    GLfloat vertex_data[] = {
        0.5f,  0.5f, 0.0f, 0.0f, 0.5f, 1.0f,
        0.5f, -0.5f, 0.0f, 0.0f, 0.5f, 1.0f,
        -0.5f, -0.5f, 0.0f, 0.0f, 0.5f, 1.0f,
        -0.5f,  0.5f, 0.0f, 0.0f, 0.5f, 1.0f,
    };

    // vertex indices
    GLuint vertex_indices[] = {
        0, 1, 3,
        1, 2, 3
    };

    // Create Vertex Buffer Object (VBO)
    glGenBuffers(1, &vbo);
    // Create Element Buffer Object (EBO)
    glGenBuffers(1, &ebo);
    // Create Vertex Array Object (VAO)
    glGenVertexArrays(1, &vao);

    // Bind VAO
    glBindVertexArray(vao);

    // Bind VBO to Array buffer
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    // Set VBO data
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertex_data), vertex_data, GL_STATIC_DRAW);

    // Bind EBO
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    // Set EBO data
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(vertex_indices), vertex_indices, GL_STATIC_DRAW);

    // Set vertex attributes pointers
    // X, Y, Z vertex
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    // R, G, B color
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);

    success = true;
}

void Quad::draw() {
    // Bind vertex array
    glBindVertexArray(vao);
    // Bind vertex index
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    // Draw element using triangles
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
}