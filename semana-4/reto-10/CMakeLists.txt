cmake_minimum_required(VERSION 3.20)
project(reto10 VERSION 1.0.0)

list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/../../cmake/sdl2)

find_package(SDL2 REQUIRED)
find_package(SDL2_image REQUIRED)
find_package(OpenGL REQUIRED)
find_package(glm REQUIRED)

set(GLAD_INCLUDE_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/../../glad/include)

include_directories(${PROJECT_NAME} ${SDL2_INCLUDE_DIRS} ${SDL2_IMAGE_INCLUDE_DIRS} ${OPENGL_INCLUDE_DIRS} ${GLM_INCLUDE_DIRS} ${GLAD_INCLUDE_DIRS})

set(SCR_FILES main.cpp shader.cpp camera.cpp object.cpp program.cpp helpers.cpp)
set(OBJ_FILES objects/quad.cpp objects/quad_textured.cpp)
set(GLAD_SRC_FILES ${CMAKE_CURRENT_SOURCE_DIR}/../../glad/src/gl.c)

set(FOLDER_SHADER assets/shaders)
set(FILES_SHADER quad water)

foreach(filename ${FILES_SHADER})
    configure_file(${FOLDER_SHADER}/${filename}.vertex.glsl ${FOLDER_SHADER}/${filename}.vertex.glsl COPYONLY)
    configure_file(${FOLDER_SHADER}/${filename}.fragment.glsl ${FOLDER_SHADER}/${filename}.fragment.glsl COPYONLY)
endforeach()

add_executable(${PROJECT_NAME} ${SCR_FILES} ${OBJ_FILES} ${GLAD_SRC_FILES})
target_link_libraries(${PROJECT_NAME} ${SDL2_LIBRARIES} ${SDL2_IMAGE_LIBRARIES} ${OPENGL_LIBRARIES} ${GLM_LIBRARIES})