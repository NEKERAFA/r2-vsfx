#include <string>

#include <glm/glm.hpp>

#ifdef _WIN32
#include <SDL.h>
#include <SDL_opengl.h>
#else
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#endif

#include "object.h"
#include "shader.h"
#include "camera.h"

#ifndef PROGRAM_H
#define PROGRAM_H

class Program {
    private:
        // Quad object
        Object* quad_obj;
        // Quad Shader object
        Shader* quad_shader;

        // Water object
        Object* water_obj;
        // Water Shader object
        Shader* water_shader;

        // Camera object
        Camera* camera;

        // Camera framebuffer object
        Camera* camera_fbo;

        // Framebuffer object
        GLuint fbo;
        // Texture framebufer object
        GLuint fto;
        // Renderbuffer object
        GLuint rbo;

        // Movement
        float dFront = 0.0f;
        float dRight = 0.0f;

        // Rotation
        float yaw = -90.0f;
        float pitch = 0.0f;

        bool success;

    public:
        Program(SDL_Window* window);
        ~Program();
    
        // Is loaded succesfully
        bool isSuccess();
        // Called when receives a keyboard event
        void keyboardEvent(const SDL_KeyboardEvent& e);
        // Called when receives a mouse motion event
        void mouseMotionEvent(const SDL_MouseMotionEvent& e);
        // Update program info
        void update(const Uint64 delta_time);
        // Draw program
        void draw();
};

#endif // PROGRAM_H