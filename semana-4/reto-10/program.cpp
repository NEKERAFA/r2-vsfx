#include <iostream>

#include <glad/gl.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#ifdef _WIN32
#include <SDL.h>
#include <SDL_opengl.h>
#else
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#endif

#include "config.h"
#include "program.h"
#include "objects/quad.h"
#include "objects/quad_textured.h"


Program::Program(SDL_Window* window) : success(false) {
    // Create quad object
    quad_obj = new Quad();
    if (quad_obj == nullptr || !quad_obj->isSuccess()) {
        std::cout << "Cannot create quad object" << std::endl;
        return;
    }

    // Load quad shader
    quad_shader = new Shader("quad");
    if (quad_shader == nullptr || !quad_shader->isSuccess()) {
        std::cout << "Cannot create quad shader" << std::endl;
        return;
    }

    // Check camera
    camera = new Camera(glm::vec3(0.0f, 0.0f, 2.0f));
    if (camera == nullptr) {
        std::cout << "Cannot create camera" << std::endl;
        return;
    }

    // Check camera framebuffer
    camera_fbo = new Camera(glm::vec3(0.0f, 1.0f, 2.0f), glm::vec3(0.0f, 0.0f, 0.0f));
    if (camera == nullptr) {
        std::cout << "Cannot create camera fbo" << std::endl;
        return;
    }

    // Create framebuffer
    glGenFramebuffers(1, &fbo);
    // Bind framebuffer
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);

    // Create texture buffer
    glGenTextures(1, &fto);
    // Bind texture buffer
    glBindTexture(GL_TEXTURE_2D, fto);
    // Create texture
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, SCREEN_WIDTH/5, SCREEN_HEIGHT/5, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
    // Set filters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    // Attach texture to framebuffer
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fto, 0);

    // Create render buffer
    glGenRenderbuffers(1, &rbo);
    // Bind render buffer
    glBindRenderbuffer(GL_RENDERBUFFER, rbo);
    // Create depth and scencill renderbuffer
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, SCREEN_WIDTH/5, SCREEN_HEIGHT/5);
    // Attach render buffer to framebuffer
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo);

    // Check renderbuffer
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        std::cout << "Cannot create framebuffer" << std::endl;
        return;
    }
    // Detach framebuffer
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    // Create water object
    water_obj = new QuadTextured(fto);
    if (water_obj == nullptr || !water_obj->isSuccess()) {
        std::cout << "Cannot create water object" << std::endl;
        return;
    }

    // Create water shader
    water_shader = new Shader("water");
    if (water_shader == nullptr || !water_shader->isSuccess()) {
        std::cout << "Cannot create water shader" << std::endl;
        return;
    }

    success = true;
}

Program::~Program() {
    if (quad_obj != nullptr) delete quad_obj;
    if (quad_shader != nullptr) delete quad_shader;
    if (water_obj != nullptr) delete water_obj;
    if (water_shader != nullptr) delete water_shader;
    if (camera != nullptr) delete camera;
    if (camera_fbo != nullptr) delete camera_fbo;
    glDeleteRenderbuffers(1, &rbo);
    glDeleteTextures(1, &fto);
    glDeleteFramebuffers(1, &fbo);
}

bool Program::isSuccess() {
    return success;
}

// Camera speed
const float cam_speed = 2.0f;

void Program::keyboardEvent(const SDL_KeyboardEvent& e) {
    switch (e.type) {
        case SDL_KEYDOWN:
            switch (e.keysym.sym) {
                case SDLK_w:
                    dFront = cam_speed;
                    return;
                case SDLK_s:
                    dFront = -cam_speed;
                    return;
                case SDLK_a:
                    dRight = -cam_speed;
                    return;
                case SDLK_d:
                    dRight = cam_speed;
                    return;
            }
        case SDL_KEYUP:
            switch (e.keysym.sym) {
                case SDLK_w:
                case SDLK_s:
                    dFront = 0.0f;
                    return;
                case SDLK_a:
                case SDLK_d:
                    dRight = 0.0f;
                    return;
            }
    }
}

const float sensitivity = 0.5f;

void Program::mouseMotionEvent(const SDL_MouseMotionEvent& e) {
    yaw += (float)e.xrel * sensitivity;
    pitch -= (float)e.yrel * sensitivity;

    if (pitch >  89.0f)
        pitch =  89.0f;
    if (pitch < -89.0f)
        pitch =  -89.0f;

    camera->rotation(yaw, pitch);
}

void Program::update(const Uint64 delta_time) {
    camera->move(dFront * (float)delta_time / 1000.0f, dRight * (float)delta_time / 1000.0f);
}

void Program::draw() {
    // Dibujamos en el framebuffer el objeto de agua
    glViewport(0, 0, SCREEN_WIDTH/5, SCREEN_HEIGHT/5);
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
    glEnable(GL_DEPTH_TEST);

    // Limpiamos el buffer
    glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Usamos el shader del quad
    quad_shader->useProgram();

    // Transform del modelo
    GLuint modelId = quad_shader->getUniformLocation("model");
    glm::mat4 model = glm::mat4(1.0f);
    model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
    quad_shader->setUniformMat4(modelId, model);

    // Transform de la cámara
    GLuint viewId = quad_shader->getUniformLocation("view");
    glm::mat4 view = camera_fbo->getTransform();
    quad_shader->setUniformMat4(viewId, view);

    // Transform de la proyección
    GLuint projId = quad_shader->getUniformLocation("projection");
    glm::mat4 proj = glm::perspective(glm::radians(45.0f), (float)SCREEN_WIDTH/(float)SCREEN_HEIGHT, 0.1f, 100.0f);
    quad_shader->setUniformMat4(projId, proj);

    // Dibujamos el quad
    quad_obj->draw();

    // Reestablece el viewport
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glDisable(GL_DEPTH_TEST);
    glViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);

    // Limpiamos la pantalla
    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    // Usamos el shader de agua
    water_shader->useProgram();

    // Transform del modelo
    modelId = water_shader->getUniformLocation("model");
    model = glm::mat4(1.0f);
    model = glm::rotate(model, glm::radians(-55.0f), glm::vec3(1.0f, 0.0f, 0.0f));
    water_shader->setUniformMat4(modelId, model);

    // Transform de la cámara
    viewId = water_shader->getUniformLocation("view");
    view = camera->getTransform();
    water_shader->setUniformMat4(viewId, view);

    // Transform de la proyección
    projId = water_shader->getUniformLocation("projection");
    water_shader->setUniformMat4(projId, proj);

    // Dibujamos el objeto de agua
    water_obj->draw();

    // Disable VAO
	glBindVertexArray(0);
	// Unbind program
    glUseProgram(0);
}