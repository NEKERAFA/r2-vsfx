#include <glad/gl.h>

#ifndef OBJECT_H
#define OBJECT_H

class Object {
    protected:
        bool success;

        // Vertex buffer object
        GLuint vbo;
        // Vertex array object
        GLuint vao;
        // Element buffer object
        GLuint ebo;

    public:
        // Create object
        Object();
        // Destroy object
        virtual ~Object();

        // Is success
        bool isSuccess();
        // Draw object
        virtual void draw() = 0;
};

#endif // OBJECT_H