#include <string>

#include <glm/glm.hpp>

#ifdef _WIN32
#include <SDL.h>
#include <SDL_opengl.h>
#else
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#endif

#include "shader.h"
#include "camera.h"

#ifndef PROGRAM_H
#define PROGRAM_H

class Program {
    private:
        // Name of file shader
        inline static const std::string FILE_SHADER = "water";

        // Vertex Buffer Object
        GLuint VBO;
        // Vertex Array Object
        GLuint VAO;
        // Element Buffer Object
        GLuint EBO;

        // Model Matrix Uniform
        GLuint modelId;
        // Model Matrix Matrix
        glm::mat4 model;

        // View Matrix Uniform
        GLuint viewId;
        // GL Camera object
        Camera* camera;

        // Projection Matrix Uniform
        GLuint projectionId;
        // Projection Matrix Matrix
        glm::mat4 projection;

        // GL Shader object
        Shader* shader;

        // Model position
        glm::vec3 model_position;

        // Movement
        float dFront = 0.0f;
        float dRight = 0.0f;

        // Rotation
        float yaw = -90.0f;
        float pitch = 0.0f;

        bool success;

    public:
        Program(SDL_Window* window);
        ~Program();
    
        // Is loaded succesfully
        bool isSuccess();
        // Called when receives a keyboard event
        void keyboardEvent(const SDL_KeyboardEvent& e);
        // Called when receives a mouse motion event
        void mouseMotionEvent(const SDL_MouseMotionEvent& e);
        // Update program info
        void update(const Uint64 delta_time);
        // Draw program
        void draw();
};

#endif // PROGRAM_H