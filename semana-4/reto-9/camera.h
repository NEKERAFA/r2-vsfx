#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#ifndef CAMERA_H
#define CAMERA_H

class Camera {
    private:
        glm::vec3 position;
        glm::vec3 front;
        glm::vec3 up;

    public:
        // Creates a camera in the world position, point to other position
        Camera(const glm::vec3& position, const glm::vec3& point_to);
        // Creates a camera in the world position
        Camera(const glm::vec3& position);
        ~Camera() = default;

        /// @brief Moves the camera
        /// @param dFront delta front
        /// @param dRight delta right
        void move(const float dFront, const float dRight);
        /// @brief Set the rotation of the camera
        /// @param yaw current yaw
        /// @param pitch current pitch
        void rotation(const float yaw, const float pitch);
        // Gets transform matrix
        glm::mat4 getTransform();
};

#endif // CAMERA_H