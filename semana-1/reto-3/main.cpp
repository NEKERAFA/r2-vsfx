#include <iostream>

#include <glad/gl.h>
#include <GLFW/glfw3.h>

#ifdef _WIN32
#include <SDL.h>
#include <SDL_opengl.h>
#else
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#endif

#include "config.h"

// Milisegundos por frame
const int TICKS_PER_FRAME = 1000 / FRAMES_PER_SECOND;

bool init_window(SDL_Window*& window, SDL_GLContext& glContext) {
    // Se inicializa el módulo SDL
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        std::cerr << "SDL could not initalize! SDL_Error: " << SDL_GetError() << std::endl;
        return false;
    }

    // Se establece el valor para los atributos de OpenGL
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

    // Se crea la ventana
    window = SDL_CreateWindow(WINDOW_TITLE, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
    if (window == NULL) {
        std::cerr << "Window could not be created! SDL_Error: " << SDL_GetError() << std::endl;
        return false;
    }

    // Se crea el contexto para OpenGL
    glContext = SDL_GL_CreateContext(window);
    if (glContext == NULL) {
        std::cerr << "Could not created OpenGL context! SDL_Error: " << SDL_GetError() << std::endl;
        return false;
    }

    // Se carga el cargador de SDL/OpenGL
    int version = gladLoadGL((GLADloadfunc) SDL_GL_GetProcAddress);
    if (version == 0) {
        std::cout << "Failed to initialize OpenGL context" << std::endl;
        return false;
    }
    std::cout << "Using OpenGL " << GLAD_VERSION_MAJOR(version) << "." << GLAD_VERSION_MINOR(version) << std::endl;

    // Se establece las dimensiones del viewport
    //glViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);

    return true;
}

void free_window(SDL_Window* window, SDL_GLContext glContext) {
    // Se elimina el contexto de OpenGL
    SDL_GL_DeleteContext(glContext);

    // Se destruye la ventana
    SDL_DestroyWindow(window);

    // Finalizamos los subsistemas de SDL
    SDL_Quit();
}

float vertices[] = {
    -0.5f, -0.5f, 0.0f,
     0.5f, -0.5f, 0.0f,
     0.0f,  0.5f, 0.0f
};

const char* vertexShaderSource =
    "#version 330 core\n\
    layout (location = 0) in vec3 aPos;\n\
    \n\
    void main() {\n\
        gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0);\n\
    }";

const char* fragmentShaderSource =
    "#version 330 core\n\
    out vec4 FragColor;\n\
    \n\
    void main() {\n\
        FragColor = vec4(1.0f, 0.5f, 0.2f, 1.0f);\n\
    }";

bool init_glProgram(u_int& VAO, u_int& vertex_shader, u_int& fragment_shader, u_int& shader_program) {
    // Create Vertex Buffer Object (VBO)
    u_int VBO;
    glGenBuffers(1, &VBO);
    // Create Vertex Array Object (VAO)
    glGenVertexArrays(1, &VAO);
    
    // Bind VAO
    glBindVertexArray(VAO);
    // Bind VBO to Array buffer
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    // Set VBO data
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    // Set vertex attributes pointers
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0); 

    // Create new Vertex Shader
    vertex_shader = glCreateShader(GL_VERTEX_SHADER);
    // Compile Vertex Shader
    glShaderSource(vertex_shader, 1, &vertexShaderSource, NULL);
    glCompileShader(vertex_shader);
    // Check if shader is compiled
    int success;
    char infoLog[512];
    glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(vertex_shader, 512, NULL, infoLog);
        std::cout << "Error on vertex shader: " << infoLog << std::endl;
        return false;
    }

    // Create new Fragment Shader
    fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
    // Compile Fragment Shader
    glShaderSource(fragment_shader, 1, &fragmentShaderSource, NULL);
    glCompileShader(fragment_shader);
    // Check if shader is compiled
    glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(fragment_shader, 512, NULL, infoLog);
        std::cout << "Error on fragment shader: " << infoLog << std::endl;
        return false;
    }

    // Create shader program
    shader_program = glCreateProgram();
    // Link shaders to program
    glAttachShader(shader_program, vertex_shader);
    glAttachShader(shader_program, fragment_shader);
    glLinkProgram(shader_program);
    // Check if shader program is compiled
    glGetShaderiv(shader_program, GL_LINK_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(shader_program, 512, NULL, infoLog);
        std::cout << "Error on program: " << infoLog << std::endl;
        return false;
    }

    return true;
}

void free_glProgram(u_int& vertex_shader, u_int& fragment_shader, u_int& shader_program) {
    glUseProgram(shader_program);
    glDeleteShader(vertex_shader);
    glDeleteShader(fragment_shader);
}

int main(int argc, char* args[]) {
    // Ventana donde se renderizará el contenido
    SDL_Window* window;
    // Contexto de OpenGL
    SDL_GLContext glContext;
    // Vertex Array Object
    u_int VAO;
    // Vertex shader
    u_int vertex_shader;
    // Fragment shader
    u_int fragment_shader;
    // Shader program
    u_int shader_program;

    // Se inicializa la ventana
    bool success = init_window(window, glContext);
    if (success) {
        // Se inicializa el programa de OpenGL
        success = init_glProgram(VAO, vertex_shader, fragment_shader, shader_program);
        if (success) {
            // Inicializamos el bucle principal
            bool quit = false;
            Uint64 last_time = SDL_GetTicks64();
            Uint64 current_time = 0ul;
            Uint64 delta_time = 0ul;
            SDL_Event event;
        
            while (!quit) {
                // Comprobamos los eventos
                while (SDL_PollEvent(&event)) {
                    switch(event.type) {
                        case SDL_QUIT:
                            quit = true;
                            break;
                        case SDL_KEYUP:
                            if (event.key.keysym.sym == SDLK_ESCAPE) {
                                quit = true;
                            }
                            break;
                        default:
                            break;
                    }
                }

                // Se ejecuta el programa
                if (!quit) {
                    // Calculamos el tiempo transcurrido desde el último frame
                    current_time = SDL_GetTicks64();
                    delta_time = current_time - last_time;

                    // Limpiamos el buffer de OpenGL
                    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
                    glClear(GL_COLOR_BUFFER_BIT);

                    // Establecemos el prorgama
                    glUseProgram(shader_program);
                    glBindVertexArray(VAO);
                    glDrawArrays(GL_TRIANGLES, 0, 3);

                    // Se actualiza el buffer de la ventana
                    SDL_GL_SwapWindow(window);
                    
                    // Esperamos el tiempo entre frames
                    if (delta_time < TICKS_PER_FRAME) {
                        SDL_Delay(TICKS_PER_FRAME - delta_time);
                    }
                    last_time = current_time;
                }
            }
            // Destruimos el programa de OpenGL
            free_glProgram(vertex_shader, fragment_shader, shader_program);
        }
        // Destruimos la ventana y se finaliza SDL
        free_window(window, glContext);
    }

    return success ? EXIT_SUCCESS : EXIT_FAILURE;
}