#ifdef _WIN32
#include <SDL.h>
#include <SDL_opengl.h>
#else
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#endif

// Título de la ventana
constexpr char WINDOW_TITLE[] = "Reto 2 - SDL & OpenGL";

// Frames por segundo
const int FRAMES_PER_SECOND = 60;

// Tamaño de la ventana
const GLuint SCREEN_WIDTH = 640;
const GLint SCREEN_HEIGHT = 480;
