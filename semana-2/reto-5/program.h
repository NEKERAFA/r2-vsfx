#include <string>

#ifdef _WIN32
#include <SDL.h>
#include <SDL_opengl.h>
#include <glm.hpp>
#else
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <glm/glm.hpp>
#endif

#include "shader.h"

#ifndef PROGRAM_H
#define PROGRAM_H

class Program {
    protected:
        bool success;

    public:
        virtual ~Program() = default;
    
        // Is loaded succesfully
        virtual bool isSuccess();
        // Update program info
        virtual void update(Uint64 delta_time) = 0;
        // Draw program
        virtual void draw() = 0;
};

class ColoredTriangle : public Program {
    private:
        // Name of file shader
        inline static const std::string FILE_SHADER = "triangle";
        // Vertex Buffer Object
        GLuint VBO;
        // Vertex Array Object
        GLuint VAO;
        // GL Shader object
        Shader* shader;

    public:
        // Create a program to show colored triangle
        ColoredTriangle();
        ~ColoredTriangle();

        // Update program info
        void update(Uint64 delta_time) override;
        // Draw program
        void draw() override;
};

class AnimatedColoredTriangle : public Program {
    private:
        // Name of file shader
        inline static const std::string FILE_SHADER = "triangleUniform";
        // Vertex Buffer Object
        GLuint VBO;
        // Vertex Array Object
        GLuint VAO;
        // Shader Uniform
        GLuint uniform;
        // Color variation
        glm::vec3 colorVariation;
        // GL Shader object
        Shader* shader;

    public:
        // Create a program to show an animated colored triangle
        AnimatedColoredTriangle();
        ~AnimatedColoredTriangle();

        // Update program info
        void update(Uint64 delta_time) override;
        // Draw program
        void draw() override;
};

#endif // PROGRAM_H