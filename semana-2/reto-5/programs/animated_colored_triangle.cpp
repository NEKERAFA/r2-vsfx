#include <glad/gl.h>

#ifdef _WIN32
#include <SDL.h>
#include <SDL_opengl.h>
#include <glm.hpp>
#else
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <glm/glm.hpp>
#endif

#include "../shader.h"
#include "../program.h"

// x, y, r, g, b
GLfloat vertices_data[] = {
    -0.5f, -0.5f, 1.0f, 1.0f, 0.0f,
     0.5f, -0.5f, 0.0f, 1.0f, 1.0f,
     0.0f,  0.5f, 1.0f, 0.0f, 1.0f
};

AnimatedColoredTriangle::AnimatedColoredTriangle() {
    VBO = -1;
    VAO = -1;
    uniform = -1;
    success = false;

    // Create Vertex Buffer Object (VBO)
    glGenBuffers(1, &VBO);
    // Create Vertex Array Object (VAO)
    glGenVertexArrays(1, &VAO);
    
    // Bind VAO
    glBindVertexArray(VAO);
    // Bind VBO to Array buffer
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    // Set VBO data
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices_data), vertices_data, GL_STATIC_DRAW);

    // Set vertex attributes pointers
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(2 * sizeof(float)));
    glEnableVertexAttribArray(1);

    // Create shader program
    shader = new Shader(FILE_SHADER);
    success = shader != nullptr && shader->getProgramID() != -1;

    if (success) {
        uniform = glGetUniformLocation(shader->getProgramID(), "colorVariation");
    }
}

AnimatedColoredTriangle::~AnimatedColoredTriangle() {
    glDeleteVertexArrays(sizeof(VAO), &VAO);
	glDeleteBuffers(sizeof(VBO), &VBO);
    delete shader;
}

Uint64 current_time = 0;

void AnimatedColoredTriangle::update(Uint64 delta_time) {
    current_time += delta_time;

    // Actualizamos el color
    colorVariation.r = sin(2.0f * M_PI * current_time / 8096.0f);
    colorVariation.g = cos(2.0f * M_PI * current_time / 1024.0f);
    colorVariation.b = sin(2.0f * M_PI * current_time / 4096.0f);
}

void AnimatedColoredTriangle::draw() {
    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    // Establecemos el shader como programa
    shader->useProgram();

    // Establecemos el color
    glUniform3f(uniform, colorVariation.r, colorVariation.g, colorVariation.b);

    // Dibuja el triangulo
    glBindVertexArray(VAO);
    glDrawArrays(GL_TRIANGLES, 0, 3);

    //Diable VAO
	glBindVertexArray(0);
	//Unbind program
    glUseProgram(0);
}