#include <glad/gl.h>

#ifdef _WIN32
#include <SDL.h>
#include <SDL_opengl.h>
#else
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#endif

#include "../shader.h"
#include "../program.h"

// x, y, r, g, b
GLfloat vertex_data[] = {
    -0.5f, -0.5f, 1.0f, 0.0f, 0.0f,
     0.5f, -0.5f, 0.0f, 1.0f, 0.0f,
     0.0f,  0.5f, 0.0f, 0.0f, 1.0f
};

ColoredTriangle::ColoredTriangle() {
    success = false;

    // Create Vertex Buffer Object (VBO)
    glGenBuffers(1, &VBO);
    // Create Vertex Array Object (VAO)
    glGenVertexArrays(1, &VAO);
    
    // Bind VAO
    glBindVertexArray(VAO);
    // Bind VBO to Array buffer
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    // Set VBO data
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertex_data), vertex_data, GL_STATIC_DRAW);

    // Set vertex attributes pointers
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(2 * sizeof(float)));
    glEnableVertexAttribArray(1);

    // Create shader program
    shader = new Shader(FILE_SHADER);
    success = shader != nullptr && shader->getProgramID() != -1;
}

ColoredTriangle::~ColoredTriangle() {
    glDeleteVertexArrays(sizeof(VAO), &VAO);
	glDeleteBuffers(sizeof(VBO), &VBO);
    delete shader;
}

void ColoredTriangle::update(Uint64 delta_time) {}

void ColoredTriangle::draw() {
    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    // Establecemos el shader como programa
    shader->useProgram();

    // Dibuja el triangulo
    glBindVertexArray(VAO);
    glDrawArrays(GL_TRIANGLES, 0, 3);

    glUseProgram(0);
}