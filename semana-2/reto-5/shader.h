#include <string>

#ifdef _WIN32
#include <SDL.h>
#include <SDL_opengl.h>
#else
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#endif

#ifndef GL_SHADER_H
#define GL_SHADER_H

class Shader {
    private:
        GLuint program_id;
        bool success;

    public:
        Shader(const std::string filename);
        ~Shader();
        GLuint getProgramID();
        void useProgram();
};

#endif // GL_SHADER_H
