#version 330 core
layout (location = 0) in vec2 aPos;
layout (location = 1) in vec3 color;

uniform vec3 colorVariation;

out vec4 vertexColor;

void main() {
    gl_Position = vec4(aPos.x, aPos.y, 0.0, 1.0);
    vec3 newColor = color * colorVariation;
    vertexColor = vec4(newColor.r, newColor.g, newColor.b, 1.0);
}