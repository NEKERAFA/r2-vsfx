#ifdef _WIN32
#include <SDL.h>
#else
#include <SDL2/SDL.h>
#endif

#ifndef PROGRAM_H
#define PROGRAM_H

class Program {
    protected:
        bool success;

    public:
        virtual ~Program() = default;
    
        // Is loaded succesfully
        virtual bool isSuccess();
        // Update program info
        virtual void update(Uint64 delta_time) = 0;
        // Draw program
        virtual void draw() = 0;
};

#endif // PROGRAM_H