#include <string>

#ifdef _WIN32
#include <SDL.h>
#include <SDL_opengl.h>
#else
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#endif

#include "../shader.h"
#include "../program.h"

#ifndef WALL_PROGRAM_H
#define WALL_PROGRAM_H

class WallProgram : public Program {
    private:
        // Name of file shader
        inline static const std::string FILE_SHADER = "wall";
        // Name of file texture
        inline static const std::string FILE_TEXTURE = "wall.jpg";
        // Vertex Buffer Object
        GLuint VBO;
        // Vertex Array Object
        GLuint VAO;
        // Element Buffer Object
        GLuint EBO;
        // Image object
        GLuint texture;
        // GL Shader object
        Shader* shader;

    public:
        // Create a program to show colored triangle
        WallProgram(SDL_Window* window);
        ~WallProgram();

        // Update program info
        void update(Uint64 delta_time) override;
        // Draw program
        void draw() override;
};

#endif // WALL_PROGRAM_H