#include <glad/gl.h>

#ifdef _WIN32
#include <SDL.h>
#include <SDL_opengl.h>
#include <SDL_image.h>
#else
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <SDL2/SDL_image.h>
#endif

#include "../program.h"
#include "../helpers.h"
#include "wall_program.h"

// x, y, r, g, b, u, v
GLfloat vertex_data[] = {
     0.5f,  0.5f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
     0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f,
    -0.5f, -0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
    -0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
};

GLuint indices[] = {
    0, 1, 3,
    1, 2, 3
};

WallProgram::WallProgram(SDL_Window* window) {
    success = false;

    // Load texture
    texture = helpers::load_image(window, FILE_TEXTURE, success);
    if (!success) return;

    // Create Vertex Buffer Object (VBO)
    glGenBuffers(1, &VBO);
    // Create Element Buffer Object (EBO)
    glGenBuffers(1, &EBO);
    // Create Vertex Array Object (VAO)
    glGenVertexArrays(1, &VAO);
    
    // Bind VAO
    glBindVertexArray(VAO);
    // Bind VBO to Array buffer
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    // Set VBO data
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertex_data), vertex_data, GL_STATIC_DRAW);

    // Bind EBO
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    // Set vertex attributes pointers
    // X, Y vertex
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 7 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    // R, G, B color
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 7 * sizeof(float), (void*)(2 * sizeof(float)));
    glEnableVertexAttribArray(1);
    // U, V texture
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 7 * sizeof(float), (void*)(5 * sizeof(float)));
    glEnableVertexAttribArray(2);

    // Create shader program
    shader = new Shader(FILE_SHADER);
    if (shader != nullptr) return;

    success = shader->getProgramID() != -1;
}

WallProgram::~WallProgram() {
    delete shader;
    glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &EBO);
}

void WallProgram::update(Uint64 delta_time) {

}

void WallProgram::draw() {
    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    // Establecemos el shader como programa
    shader->useProgram();

    // Estable la textura
    //glBindTexture(GL_TEXTURE_2D, texture);

    // Dibuja la textura
    glBindVertexArray(VAO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

    // Disable VAO
	glBindVertexArray(0);
	// Unbind program
    glUseProgram(0);
}