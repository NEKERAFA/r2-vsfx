#include <iostream>
#include <string>
#include <filesystem>

#include <glad/gl.h>

#ifdef _WIN32
#include <SDL.h>
#include <SDL_image.h>
#else
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#endif

#include "config.h"
#include "texture.h"

namespace fs = std::filesystem;

Texture::Texture() : success(false) {}

Texture::~Texture() {
    if (success) {
        glDeleteTextures(1, &id);
        std::cout << "Texture disposed" << std::endl;
    }
}

bool Texture::load(const std::string& filename) {
    // Get image file
    fs::path file_image = ".";
    file_image /= DIRECTORY_TEXTURES;
    file_image /= filename;

    // Load image at specified path
    SDL_Surface* loaded_image = IMG_Load(file_image.c_str());
    if (loaded_image == NULL) {
        std::cerr << "Unable to load image " << file_image << "! SDL_Image Error: " << IMG_GetError() << std::endl;
        return 0;
    }
    std::cout << file_image << " loaded sucessfully!" << std::endl;

    // Generate OpenGL texture
    glGenTextures(1, &id);
    glBindTexture(GL_TEXTURE_2D, id);

    // Load image as texture
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, loaded_image->w, loaded_image->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, loaded_image->pixels);
    // Generate mipmap
    glGenerateMipmap(GL_TEXTURE_2D);

    // Free texture
    SDL_FreeSurface(loaded_image);

    return success = id > 0;
}

bool Texture::isSuccess() {
    return success;
}

void Texture::use() {
    if (success) glBindTexture(GL_TEXTURE_2D, id);
}