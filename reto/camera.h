#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#ifndef CAMERA_H
#define CAMERA_H

class Camera {
    private:
        glm::vec3 position;
        glm::vec3 front;
        glm::vec3 up;

    public:
        Camera();
        ~Camera() = default;

        /// @brief Set a camera pointing to other position
        /// @param position position of the camera
        /// @param point_to point to look
        void lookAt(const glm::vec3& position, const glm::vec3& point_to);
        /// @brief Set a camera pointing to front (-z)
        /// @param position position of the camera
        void move(const glm::vec3& position);
        /// @brief Moves the camera
        /// @param dFront delta front
        /// @param dRight delta right
        void move(const float dFront, const float dRight);
        /// @brief Set the rotation of the camera
        /// @param yaw current yaw
        /// @param pitch current pitch
        void rotation(const float yaw, const float pitch);

        // Gets transform matrix
        glm::mat4 getTransform();
};

#endif // CAMERA_H