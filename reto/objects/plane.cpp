#include <glad/gl.h>

#include "../object.h"
#include "plane.h"

Plane::Plane() : Object() {
    // vertex data as (x, y, z, u, v)
    GLfloat vertex_data[] = {
         0.5f,  0.5f, 0.0f, 1.0f, 1.0f,
         0.5f, -0.5f, 0.0f, 1.0f, 0.0f,
        -0.5f, -0.5f, 0.0f, 0.0f, 0.0f,
        -0.5f,  0.5f, 0.0f, 0.0f, 1.0f,
    };

    // vertex indices
    GLuint vertex_indices[] = {
        0, 3, 1,
        1, 3, 2
    };

    // Se crea un vertex buffer
    glGenBuffers(1, &vbo);
    // Se crea un index buffer
    glGenBuffers(1, &ebo);

    // Se crea el vertex array
    glGenVertexArrays(1, &vao);
    // Se bindea el vertex array
    glBindVertexArray(vao);

    // Se bindea el vertex buffer al array buffer
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    // Se establece los datos del vertex buffer
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertex_data), vertex_data, GL_STATIC_DRAW);

    // Se bindea el index buffer al array buffer
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    // Se establece los datos del index buffer
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(vertex_indices), vertex_indices, GL_STATIC_DRAW);

    // Se establecen el atributo (x, y, z) del vertex buffer
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    // Se establece el atributo (u, v)
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);

    // Se unblindea el vertex buffer
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // Se unblindea el vertex array
    glBindVertexArray(0);

    success = shader.load("plane");
}

void Plane::draw() {
    // Se blindea el vertex array
    glBindVertexArray(vao);
    // Se blindea el index buffer
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    // Se dibuja la mesh
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
    // Se unblindea el vertex array
    glBindVertexArray(0);
}