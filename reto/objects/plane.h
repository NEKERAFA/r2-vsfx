#include <glad/gl.h>

#include "../object.h"

#ifndef PLANE_OBJECT_H
#define PLANE_OBJECT_H

class Plane : public Object {
    public:
        // Create plane
        Plane();
        // Destroy object
        ~Plane() = default;

        // Draw plane
        void draw() override;
};

#endif // PLANE_OBJECT_H