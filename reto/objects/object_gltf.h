#include <string>

#include <glad/gl.h>
#include <glm/glm.hpp>

#include "tiny_gltf.h"

#include "../object.h"

#ifndef OBJECT_GLTF_H
#define OBJECT_GLTF_H

class ObjectGltf : public Object {
    private:
        // Model
        tinygltf::Model model;

    public:
        // Create object
        ObjectGltf();
        // Destroy object
        ~ObjectGltf() = default;

        // Load a gltf object
        bool load(const std::string& filename);
        // Draw object
        void draw() override;

        const glm::vec3 getVec3Max();
};

#endif // OBJECT_GLTF_H