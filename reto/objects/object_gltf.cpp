#include <iostream>
#include <string>
#include <filesystem>
#include <glad/gl.h>

#include "tiny_gltf.h"

#include "../config.h"
#include "../shader.h"
#include "../object.h"
#include "object_gltf.h"

namespace fs = std::filesystem;

void load_glModel(const std::string& filename, tinygltf::Model& model, bool& success) {
    tinygltf::TinyGLTF loader;
    std::string err;
    std::string warn;

    // Get model file
    std::string fullname = filename + ".gltf";
    fs::path file_object = ".";
    file_object /= DIRECTORY_MODELS;
    file_object /= fullname;

    bool ret = loader.LoadASCIIFromFile(&model, &err, &warn, file_object);
    
    if (!warn.empty())
        std::cout << "Warning: " << warn << std::endl;

    if (!err.empty()) {
        std::cerr << "Error: " << err << std::endl;
        success = false;
        return;
    }

    if (!ret) {
        std::cerr << "Cannot load " << file_object << std::endl;
        success = false;
    }

    std::cout << file_object << " loaded successfully!" << std::endl;
    success = true;
}

// Para cargar una mesh, tenemos que seguir la info de como está un fichero gltf
// https://www.khronos.org/files/gltf20-reference-guide.pdf
void load_glMesh(tinygltf::Model& model, GLuint& vao, GLuint& vbo, GLuint& ebo) {
    auto mesh = model.meshes[0]; // Solo voy a cargar el primer mesh
    auto primitive = mesh.primitives[0]; // Se obtiene la primera primitiva
    auto vertexAccessor = model.accessors[primitive.attributes["POSITION"]]; // Se obtiene el accessor del vertex buffer
    auto indexAccessor = model.accessors[primitive.indices]; // Se obtiene el accessor del index buffer
    auto vertexBuffer = model.bufferViews[vertexAccessor.bufferView]; // Se obtiene el vertex buffer
    auto indexBuffer = model.bufferViews[indexAccessor.bufferView]; // Se obtiene el index buffer
    auto dataBuffer = &model.buffers[0].data.at(0); // buffer con los datos en bruto del modelo

    // Debug
    // const float* positions = reinterpret_cast<const float*>(&model.buffers[0].data[vertexBuffer.byteOffset + vertexAccessor.byteOffset]);
    // std::cout << "count: " << vertexAccessor.count << std::endl;
    // for (size_t i = 0; i < vertexAccessor.count; i++) {
    //     std::cout << "(" << positions[i * 3 + 0] << "," << positions[i * 3 + 1] << ", " << positions[i * 3 + 2] << ")" << std::endl;
    // }

    // if (primitive.mode == TINYGLTF_MODE_TRIANGLES) {
    //   std::cout << "GL_TRIANGLES" << std::endl;
    // } else if (primitive.mode == TINYGLTF_MODE_TRIANGLE_STRIP) {
    //   std::cout << "GL_TRIANGLES_STRIP" << std::endl;
    // } else if (primitive.mode == TINYGLTF_MODE_TRIANGLE_FAN) {
    //   std::cout << "GL_TRIANGLES_STRIP" << std::endl;
    // }

    // Se crea un vertex buffer
    glGenBuffers(1, &vbo);
    // Se crea un index buffer
    glGenBuffers(1, &ebo);

    // Se crea el vertex array
    glGenVertexArrays(1, &vao);
    // Se bindea el vertex array
    glBindVertexArray(vao);

    // Se bindea el vertex buffer al array buffer
    glBindBuffer(vertexBuffer.target, vbo);
    // Se establece los datos del vertex buffer
    glBufferData(vertexBuffer.target, vertexBuffer.byteLength, dataBuffer + vertexBuffer.byteOffset, GL_STATIC_DRAW);

    // Se bindea el index buffer al array buffer
    glBindBuffer(indexBuffer.target, ebo);
    // Se establece los datos del index buffer
    glBufferData(indexBuffer.target, indexBuffer.byteLength, dataBuffer + indexBuffer.byteOffset, GL_STATIC_DRAW);

    // Se establecen el atributo del vertex buffer
    glVertexAttribPointer(primitive.attributes["POSITION"], 3, vertexAccessor.componentType, GL_FALSE, vertexAccessor.ByteStride(vertexBuffer), (void*)0);
    glEnableVertexAttribArray(primitive.attributes["POSITION"]);

    // Se unblindea el vertex buffer
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // Se unblindea el vertex array
    glBindVertexArray(0);
}

ObjectGltf::ObjectGltf() : Object() {}

bool ObjectGltf::load(const std::string& filename) {
    // Se carga el modelo
    load_glModel(filename, model, success);
    // Se carga la mesh
    load_glMesh(model, vao, vbo, ebo);
    success = shader.load(filename);
    return success;
}

void ObjectGltf::draw() {
    // Se blindea el vertex array
    glBindVertexArray(vao);
    // Se blindea el index buffer
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    // Se obtiene la primitiva del vertex buffer
    auto primitive = model.meshes[0].primitives[0];
    int mode = -1; // Modo de dibujado
    if (primitive.mode == TINYGLTF_MODE_TRIANGLES) {
      mode = GL_TRIANGLES; // Triángulos
    } else if (primitive.mode == TINYGLTF_MODE_TRIANGLE_STRIP) {
      mode = GL_TRIANGLE_STRIP;
    } else if (primitive.mode == TINYGLTF_MODE_TRIANGLE_FAN) {
      mode = GL_TRIANGLE_FAN;
    }
    // Se obtiene el accessor del vertex buffer
    auto indexAccessor = model.accessors[primitive.indices];
    // Se dibuja la mesh
    glDrawElements(mode, indexAccessor.count, indexAccessor.componentType, (void*)0);
    // Se unblindea el vertex array
    glBindVertexArray(0);
}

const glm::vec3 ObjectGltf::getVec3Max() {
    auto primitive = model.meshes[0].primitives[0];
    auto vertexAccessor = model.accessors[primitive.attributes["POSITION"]];
    auto maxValues = vertexAccessor.maxValues;
    return glm::vec3(maxValues[0], maxValues[1], maxValues[2]);
}