#include <glad/gl.h>

#include "shader.h"

#ifndef OBJECT_H
#define OBJECT_H

class Object {
    protected:
        bool success;

        // Shader
        Shader shader;

        // Vertex Array Oobject
        GLuint vao;
        // Vertex Buffer object
        GLuint vbo;
        // Element Buffer object
        GLuint ebo;

    public:
        // Create object
        Object();
        // Destroy object
        virtual ~Object();

        // Is success
        virtual bool isSuccess();
        // Usa el shader
        virtual void useShader();
        // Set mat4 to shader
        virtual void setUniformMat4(const std::string& uniform, const glm::mat4& mat) const;
        // Set float to shader
        virtual void setUniform1f(const std::string& uniform, const float value) const;
        // Draw object
        virtual void draw() = 0;
};

#endif // OBJECT_H