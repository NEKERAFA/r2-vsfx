#include <iostream>
#include <fstream>
#include <string>
#include <filesystem>

#include <glad/gl.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "config.h"
#include "shader.h"

namespace fs = std::filesystem;

const int error_size = 512;

bool glCheckErrors(const GLuint shader_id, const unsigned int shader_type, const std::string& shader_name) {
    int success;
    glGetShaderiv(shader_id, shader_type, &success);
    if (!success) {
        char* error_msg = new char[error_size];
        glGetShaderInfoLog(shader_id, error_size, NULL, error_msg);
        std::cout << "Error on " << shader_name << ": " << error_msg << std::endl;
        delete[] error_msg;
        return true;
    }

    return false;
}

GLuint load_glShader(const std::string& filename, const unsigned int shader_type, bool& success) {
    char* buffer = new char[CODE_BUFFER_SIZE]();

    std::string shader_name = shader_type == GL_VERTEX_SHADER ? "vertex" : "fragment";

    // Get vertex file
    std::string fullname = filename + "." + shader_name + ".glsl";
    fs::path file_shader = ".";
    file_shader /= DIRECTORY_SHADERS;
    file_shader /= fullname;

    // Open vertex file
    std::fstream source_file;
    source_file.open(file_shader, std::ios::in | std::ios::binary);
    if (!(source_file.is_open())) {
        std::cerr << "Cannot load " << file_shader << std::endl;
        delete[] buffer;
        success = false;
        return 0;
    }

    // Read vertex file into buffer
    source_file.read(buffer, CODE_BUFFER_SIZE);
    source_file.close();

    // Create new vertex shader
    GLuint shader = glCreateShader(shader_type);

    // Compile vertex shader
    glShaderSource(shader, 1, &buffer, NULL);
    glCompileShader(shader);
    if (glCheckErrors(shader, GL_COMPILE_STATUS, shader_name)) {
        success = false;
        delete[] buffer;
        return 0;
    }

    delete[] buffer;
    success = true;
    std::cout << file_shader << " loaded successfully!" << std::endl;
    return shader;
}

GLuint create_glProgram(const GLuint vertex_shader, const GLuint fragment_shader, bool& success) {
    // Create shader program
    GLuint shader_program = glCreateProgram();
    // Link shaders to program
    glAttachShader(shader_program, vertex_shader);
    glAttachShader(shader_program, fragment_shader);
    glLinkProgram(shader_program);
    if (glCheckErrors(shader_program, GL_LINK_STATUS, "program")) {
        success = false;
        return 0;
    }

    success = true;
    return shader_program;
}

Shader::Shader() : success(false), id(0) {}

bool Shader::load(const std::string& filename) {
    // Load vertex shader
    GLuint vertex_shader = load_glShader(filename, GL_VERTEX_SHADER, success);
    if (success) {
        GLuint fragment_shader = load_glShader(filename, GL_FRAGMENT_SHADER, success);
        if (success) {
            id = create_glProgram(vertex_shader, fragment_shader, success);
            glDeleteShader(fragment_shader);
        }
        glDeleteShader(vertex_shader);
    }

    return isSuccess();
}

Shader::~Shader() {
    if (success) {
        glDeleteProgram(id);
        std::cout << "Shader disposed" << std::endl;
    }
}

GLuint Shader::getId() const {
    return id;
}

void Shader::use() const {
    if (success) glUseProgram(id);
}

bool Shader::isSuccess() const {
    return success && id > 0;
}

GLuint Shader::getUniform(const std::string& uniform) const {
    return glGetUniformLocation(id, uniform.c_str());
}

void Shader::setUniformMat4(const GLuint uniform, const glm::mat4& mat) const {
    glUniformMatrix4fv(uniform, 1, GL_FALSE, glm::value_ptr(mat));
}

void Shader::setUniform1f(const GLuint uniform, const float value) const {
    glUniform1f(uniform, value);
}
