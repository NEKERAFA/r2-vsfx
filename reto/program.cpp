#include <iostream>

#include <glad/gl.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#ifdef _WIN32
#include <SDL.h>
#include <SDL_opengl.h>
#else
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#endif

#include "config.h"
#include "program.h"

Program::Program(SDL_Window* window) : success(false) {
    // Load grass
    if (!grass.load("grass.png")) return;

    // Load flame object
    if (!flame.load("flame")) return;

    // Check camera
    camera.move(glm::vec3(0.0f, 1.25f, 4.0f));

    // Check camera framebuffer
    camera_fbo.lookAt(glm::vec3(0.0f, 0.0f, 2.0f), glm::vec3(0.0f, 0.0f, 0.0f));

    // Create framebuffer
    glGenFramebuffers(1, &fbo);
    // Bind framebuffer
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);

    // Create texture buffer
    glGenTextures(1, &fto);
    // Bind texture buffer
    glBindTexture(GL_TEXTURE_2D, fto);
    // Create texture
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, SCREEN_WIDTH/5, SCREEN_HEIGHT/5, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
    // Set filters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    // Attach texture to framebuffer
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fto, 0);

    // Create render buffer
    glGenRenderbuffers(1, &rbo);
    // Bind render buffer
    glBindRenderbuffer(GL_RENDERBUFFER, rbo);
    // Create depth and scencill renderbuffer
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, SCREEN_WIDTH/5, SCREEN_HEIGHT/5);
    // Attach render buffer to framebuffer
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo);

    // Check renderbuffer
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        std::cout << "Cannot create framebuffer" << std::endl;
        return;
    }
    // Detach framebuffer
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    // Enable Z Buffer (Depth buffer)
    glEnable(GL_DEPTH_TEST);
    // Enable texture blending
    glEnable(GL_BLEND);
    // Set blending mode 
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    // Enable cull face
    glEnable(GL_CULL_FACE);
    // Set cull facing
    glCullFace(GL_FRONT);
    glFrontFace(GL_CW);

    success = true;
}

Program::~Program() {
    glDeleteRenderbuffers(1, &rbo);
    glDeleteTextures(1, &fto);
    glDeleteFramebuffers(1, &fbo);
}

bool Program::isSuccess() {
    return success;
}

// Camera speed
const float cam_speed = 2.0f;

void Program::keyboardEvent(const SDL_KeyboardEvent& e) {
    switch (e.type) {
        case SDL_KEYDOWN:
            switch (e.keysym.sym) {
                case SDLK_w:
                    dFront = cam_speed;
                    return;
                case SDLK_s:
                    dFront = -cam_speed;
                    return;
                case SDLK_a:
                    dRight = -cam_speed;
                    return;
                case SDLK_d:
                    dRight = cam_speed;
                    return;
            }
        case SDL_KEYUP:
            switch (e.keysym.sym) {
                case SDLK_w:
                case SDLK_s:
                    dFront = 0.0f;
                    return;
                case SDLK_a:
                case SDLK_d:
                    dRight = 0.0f;
                    return;
            }
    }
}

const float sensitivity = 0.5f;

void Program::mouseMotionEvent(const SDL_MouseMotionEvent& e) {
    yaw += (float)e.xrel * sensitivity;
    pitch -= (float)e.yrel * sensitivity;

    if (pitch >  89.0f)
        pitch =  89.0f;
    if (pitch < -89.0f)
        pitch =  -89.0f;

    camera.rotation(yaw, pitch);
}

void Program::update(const Uint64 time, const Uint32 delta_time) {
    this->time = time;
    camera.move(dFront * static_cast<float>(delta_time) / 1000.0f, dRight * static_cast<float>(delta_time) / 1000.0f);
}

void Program::draw() {
    // Limpiamos la pantalla
    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Transform de la cámara
    glm::mat4 view = camera.getTransform();
    // Transform de la proyeccion
    glm::mat4 projection = glm::perspective(glm::radians(45.0f), static_cast<float>(SCREEN_WIDTH)/static_cast<float>(SCREEN_HEIGHT), 0.1f, 100.0f);

    // Se establece el shader del plano
    plane.useShader();

    // Se establece la textura
    grass.use();

    // Se pone el valor del tiempo
    plane.setUniform1f("time", static_cast<float>(time) / 1000.0f);

    // Se establece las matrices de transformación
    glm::mat4 plane_trans = glm::mat4(1.0f);
    plane_trans = glm::rotate(plane_trans, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
    plane_trans = glm::scale(plane_trans, glm::vec3(4.0f, 4.0f, 4.0f));
    plane.setUniformMat4("model", plane_trans);
    plane.setUniformMat4("view", view);
    plane.setUniformMat4("projection", projection);

    // Dibujamos el plano
    plane.draw();

    // Limpiamos la textura
    glBindTexture(GL_TEXTURE_2D, 0);

    // Se establece el shader de la llama
    flame.useShader();

    // Se pone el valor del tiempo
    flame.setUniform1f("time", static_cast<float>(time) / 1000.0f);

    // Se pone el valor más alto de la llama
    flame.setUniform1f("yMax", flame.getVec3Max().y);

    // Se establece las matrices de transformación
    glm::mat4 flame_trans = glm::mat4(1.0f);
    flame.setUniformMat4("model", flame_trans);
    flame.setUniformMat4("view", view);
    flame.setUniformMat4("projection", projection);

    // Dibujamos la llama
    flame.draw();

    // Deshabilitamos el vertex array
	glBindVertexArray(0);
	// Limpiamos el shader
    glUseProgram(0);
}