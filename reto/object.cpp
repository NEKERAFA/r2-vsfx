#include <iostream>

#include <glad/gl.h>

#include "shader.h"
#include "object.h"

Object::Object() : success(false) {}

Object::~Object() {
    if (success) {
        glDeleteVertexArrays(1, &vao);
        glDeleteBuffers(1, &vbo);
        glDeleteBuffers(1, &ebo);
        std::cout << "Object disposed" << std::endl;
    }
}

bool Object::isSuccess() {
    return success;
}

void Object::setUniformMat4(const std::string& uniform, const glm::mat4& mat) const {
    shader.setUniformMat4(shader.getUniform(uniform), mat);
}

void Object::setUniform1f(const std::string& uniform, const float value) const {
    shader.setUniform1f(shader.getUniform(uniform), value);
}

void Object::useShader() {
    // Se establece el shader
    shader.use();
}