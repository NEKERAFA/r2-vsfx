#include <string>

#include <glad/gl.h>

#ifndef TEXTURE_H
#define TEXTURE_H

class Texture {
    private:
        bool success;
        GLuint id;

    public:
        Texture();
        ~Texture();

        bool load(const std::string& filename);
        bool isSuccess();
        void use();
};

#endif // TEXTURE_H