#include <glad/gl.h>

// Título de la ventana
constexpr char WINDOW_TITLE[] = "Reto 2 - Fuego en el shader";

// Frames por segundo
const int FRAMES_PER_SECOND = 60;

// Tamaño de la ventana
const GLuint SCREEN_WIDTH = 640;
const GLuint SCREEN_HEIGHT = 480;

// Directorio con los modelos
constexpr char DIRECTORY_MODELS[] = "assets/models";

// Directorio con las texturas
constexpr char DIRECTORY_TEXTURES[] = "assets/textures";

// Directorio con los shaders
constexpr char DIRECTORY_SHADERS[] = "assets/shaders";
// Shader buffer size
const int CODE_BUFFER_SIZE = 8192;
