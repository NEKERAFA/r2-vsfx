#version 330 core
out vec4 FragColor;

in vec4 vertexColor;
in vec2 TexCoord;

// Textura del plano
uniform sampler2D aTexture;

// Tiempo del programa
uniform float time;

void main() {
    // Dejamos el suelo redondeado siguiendo la fórmula de la circunferencia
    // (x - ox)^2 + (y - oy)^2 < radio^2
    float radius = pow(TexCoord.x - 0.5, 2.0) + pow(TexCoord.y - 0.5, 2.0);
    if (radius > 0.25)
        discard;

    // Delta actual para hacer efecto de luz
    float delta = sin(time * 16.0) * cos(time * 4.0) / 2;

    // Color entre la textura y la luz
    vec4 texColor = mix(vec4(1.0, 1.0, 0.0, 1.0), vertexColor, min(0.5, (radius - 0.125) / 0.125) + delta);

    FragColor = texture(aTexture, TexCoord) * texColor;
}