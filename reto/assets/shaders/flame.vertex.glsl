#version 330 core
layout (location = 0) in vec3 aPos;

// Tiempo del programa
uniform float time;

// Altura máxima del objeto
uniform float yMax;

// Transformada del modelo
uniform mat4 model;
// Transformada de la vista
uniform mat4 view;
// Transformada de la proyección
uniform mat4 projection;

out vec4 vertexColor;

void main() {
    float height = aPos.y / yMax;

    vec3 vertexPosition = aPos;
    if (height > 0.1) {
        // delta y
        float deltaY = (height - 0.1) / 0.9;
        vec3 delta = vec3(
            sin(deltaY + time * 4.0) + cos(deltaY - time * 2.0), // plano ZY
            sin(deltaY + time * 8.0) + cos(deltaY + time * 4.0), // plano XY
            sin(deltaY - time * 2.0) + cos(deltaY + time * 8.0)  // plano XZ
        );
        vertexPosition = deltaY * 0.1 * delta + vertexPosition;
    }

    // De derecha a izquierda: P * V * M * L
    gl_Position = projection * view * model * vec4(vertexPosition, 1.0);

    // Color del vértice
    if (height < 0.1) {
        vertexColor = vec4(1.0);
    } else if (height < 0.3) {
        vertexColor = mix(vec4(1.0, 1.0, 0.0, 1.0), vec4(1.0, 0.0, 0.0, 1.0), (height - 0.1) / 0.2);
    } else {
        float alpha = (height - 0.4) / 0.6;
        vertexColor = vec4(1.0, 0.0, 0.0, 1.0 - max(alpha, 0.0));
    }
}