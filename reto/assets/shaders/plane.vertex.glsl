#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec2 aTexCoord;

// Transformada del modelo
uniform mat4 model;
// Transformada de la vista
uniform mat4 view;
// Transformada de la proyección
uniform mat4 projection;

out vec4 vertexColor;
out vec2 TexCoord;

void main() {
    // De derecha a izquierda: P * V * M * L
    gl_Position = projection * view * model * vec4(aPos, 1.0);
    // Color del vértice
    vertexColor = vec4(1.0);
    // Textura
    TexCoord = aTexCoord;
}