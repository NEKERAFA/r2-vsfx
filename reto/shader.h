#include <string>

#include <glad/gl.h>
#include <glm/glm.hpp>

#ifndef GL_SHADER_H
#define GL_SHADER_H

class Shader {
    private:
        GLuint id;
        bool success;

    public:
        Shader();
        ~Shader();
        bool load(const std::string& filename);
        GLuint getId() const;
        void use() const;
        bool isSuccess() const;
        GLuint getUniform(const std::string& uniform) const;
        void setUniformMat4(const GLuint uniform, const glm::mat4& mat) const;
        void setUniform1f(const GLuint uniform, const float value) const;
};

#endif // GL_SHADER_H
