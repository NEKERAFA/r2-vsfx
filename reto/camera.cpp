#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "camera.h"

Camera::Camera() : position(glm::vec3(0.0f, 0.0f, 0.0f)),
    front(glm::vec3(0.0f, 0.0f, -1.0f)),
    up(glm::vec3(0.0f, 1.0f, 0.0f))
{}

void Camera::lookAt(const glm::vec3& position, const glm::vec3& point_to) {
    this->position = position;
    this->front = glm::normalize(point_to - position);
}

void Camera::move(const glm::vec3& position) {
    this->position = position;
    this->front = glm::vec3(0.0f, 0.0f, -1.0f);
}

void Camera::move(const float dFront, const float dRight) {
    position += dFront * front + dRight * glm::normalize(glm::cross(front, up));
}

void Camera::rotation(const float yaw, const float pitch) {
    glm::vec3 direction;
    direction.x = cos(glm::radians(yaw) * cos(glm::radians(pitch)));
    direction.y = sin(glm::radians(pitch));
    direction.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
    front = glm::normalize(direction);
}

glm::mat4 Camera::getTransform() {
    return glm::lookAt(position, position + front, up);
}