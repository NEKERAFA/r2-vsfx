#version 330 core
layout (location = 0) in vec2 aPos;
layout (location = 1) in vec3 aColor;
layout (location = 2) in vec2 aTexCoord;

out vec4 vertexColor;
out vec2 TexCoord;

uniform mat4 transform;

void main() {
    mat4 trans = mat4(1.0);
    gl_Position = transform * vec4(aPos, 0.0, 1.0);
    vertexColor = vec4(aColor, 1.0);
    TexCoord = aTexCoord;
}