#include <string>

#ifdef _WIN32
#include <SDL.h>
#include <SDL_opengl.h>
#else
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#endif

#ifndef SDL_HELPERS_H
#define SDL_HELPERS_H

namespace helpers {
    GLuint load_image(SDL_Window* window, const std::string filename, bool& success);
}

#endif // SDL_HELPERS_H