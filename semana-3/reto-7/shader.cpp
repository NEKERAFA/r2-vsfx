#include <iostream>
#include <fstream>
#include <string>
#include <filesystem>

#include <glad/gl.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#ifdef _WIN32
#include <SDL.h>
#include <SDL_opengl.h>
#else
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#endif

#include "config.h"
#include "shader.h"

namespace fs = std::filesystem;

GLuint load_glShader(const std::string& filename, const int shader_type) {
    int success;
    char* buffer = new char[CODE_BUFFER_SIZE]();

    std::string shader_name = shader_type == GL_VERTEX_SHADER ? "vertex" : "fragment";

    // Get vertex file
    std::string fullname = filename + "." + shader_name + ".glsl";
    fs::path file_shader = ".";
    file_shader /= DIRECTORY_SHADERS;
    file_shader /= fullname;

    // Open vertex file
    std::fstream source_file;
    source_file.open(file_shader, std::ios::in | std::ios::binary);
    if (!(source_file.is_open())) {
        std::cerr << "No se puede abrir " << file_shader << std::endl;
        delete[] buffer;
        return -1;
    }

    // Read vertex file into buffer
    source_file.read(buffer, CODE_BUFFER_SIZE);
    source_file.close();

    // Create new vertex shader
    GLuint shader = glCreateShader(shader_type);

    // Compile vertex shader
    glShaderSource(shader, 1, &buffer, NULL);
    glCompileShader(shader);

    // Check if shader is compiled successfully
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (!success) {
        char* infoLog = new char[512];
        glGetShaderInfoLog(shader, 512, NULL, infoLog);
        std::cout << "Error on " << shader_name << " shader: " << infoLog << std::endl;
        glDeleteShader(shader);
        delete[] buffer;
        delete[] infoLog;
        return -1;
    }

    delete[] buffer;
    return shader;
}

GLuint create_glProgram(const GLuint vertex_shader, const GLuint fragment_shader) {
    int success;

    // Create shader program
    GLuint shader_program = glCreateProgram();
    // Link shaders to program
    glAttachShader(shader_program, vertex_shader);
    glAttachShader(shader_program, fragment_shader);
    glLinkProgram(shader_program);
    // Check if shader program is compiled
    glGetShaderiv(shader_program, GL_LINK_STATUS, &success);
    if (!success) {
        char* infoLog = new char[512];
        glGetShaderInfoLog(shader_program, 512, NULL, infoLog);
        std::cout << "Error on program: " << infoLog << std::endl;
        glDeleteProgram(shader_program);
        delete[] infoLog;
        return -1;
    }

    return shader_program;
}

Shader::Shader(const std::string& filename) {
    success = false;
    program_id = -1;

    // Load vertex shader
    GLuint vertex_shader;
    if ((vertex_shader = load_glShader(filename, GL_VERTEX_SHADER)) != -1) {
        // Load fragment shader
        GLuint fragment_shader;
        if ((fragment_shader = load_glShader(filename, GL_FRAGMENT_SHADER)) != -1) {
            // Create program
            program_id = create_glProgram(vertex_shader, fragment_shader);
        }

        glDeleteShader(vertex_shader);
        glDeleteShader(fragment_shader);
    }

    success = program_id > 0;
}

Shader::~Shader() {
    if (success) glDeleteProgram(program_id);
}

GLuint Shader::getProgramID() const {
    return program_id;
}

void Shader::useProgram() const {
    if (success) glUseProgram(program_id);
}

bool Shader::isSuccess() const {
    return success;
}

GLuint Shader::getUniformLocation(const std::string& uniform) const {
    return glGetUniformLocation(program_id, uniform.c_str());
}

void Shader::setUniformMat4(const GLuint uniform, const glm::mat4& mat) const {
    glUniformMatrix4fv(uniform, 1, GL_FALSE, glm::value_ptr(mat));
}
