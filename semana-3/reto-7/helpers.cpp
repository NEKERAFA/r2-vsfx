#include <iostream>
#include <string>
#include <filesystem>

#include <glad/gl.h>

#ifdef _WIN32
#include <SDL.h>
#include <SDL_opengl.h>
#include <SDL_image.h>
#else
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <SDL2/SDL_image.h>
#endif

#include "config.h"
#include "helpers.h"

namespace fs = std::filesystem;

GLuint helpers::load_image(SDL_Window* window, const std::string filename, bool& success) {
    success = false;

    // Get image file
    fs::path file_image = ".";
    file_image /= DIRECTORY_TEXTURE;
    file_image /= filename;

    // Load image at specified path
    SDL_Surface* loaded_image = IMG_Load(file_image.c_str());
    if (loaded_image == NULL) {
        std::cerr << "Unable to load image " << file_image << "! SDL_Image Error: " << IMG_GetError() << std::endl;
        return 0;
    }

    // Convert surface to screen format
    //SDL_Surface* surface = SDL_GetWindowSurface(window);
    /*
    SDL_Surface* optimized_image = SDL_ConvertSurface(loaded_image, surface->format, 0);
    if (optimized_image == NULL) {
        std::cerr << "Unable to optimize image " << file_image << "! SDL_Error: " << SDL_GetError() << std::endl;
    } else {
        // Get rid of old loaded surface
        SDL_FreeSurface(loaded_image);
        loaded_image = optimized_image;
        optimized_image = NULL;
    }
    */

    // Generate OpenGL texture
    GLuint texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);

    // Load image as texture
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, loaded_image->w, loaded_image->h, 0, GL_RGB, GL_UNSIGNED_BYTE, loaded_image->pixels);
    // Generate mipmap
    glGenerateMipmap(GL_TEXTURE_2D);

    // Free texture
    SDL_FreeSurface(loaded_image);

    success = true;
    return texture;
}