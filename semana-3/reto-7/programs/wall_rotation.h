#include <string>

#ifdef _WIN32
#include <SDL.h>
#include <SDL_opengl.h>
#else
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#endif

#include <glm/glm.hpp>

#include "../shader.h"
#include "../program.h"

#ifndef WALL_ROTATION_H
#define WALL_ROTATION_H

class WallRotation : public Program {
    private:
        // Name of file shader
        inline static const std::string FILE_SHADER = "wall";
        // Name of file texture
        inline static const std::string FILE_TEXTURE = "wall.jpg";
        // Vertex Buffer Object
        GLuint VBO;
        // Vertex Array Object
        GLuint VAO;
        // Element Buffer Object
        GLuint EBO;
        // Image object
        GLuint texture;
        // Shader Uniform
        GLuint transformId;
        // Transform Matrix
        glm::mat4 transform;
        // GL Shader object
        Shader* shader;

        // Rotation;
        float rotation;

    public:
        // Create a program to show colored triangle
        WallRotation(SDL_Window* window);
        ~WallRotation();

        // Update program info
        void update(Uint64 delta_time) override;
        // Draw program
        void draw() override;
};

#endif // WALL_ROTATION_H