#include <iostream>

#include <glad/gl.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#ifdef _WIN32
#include <SDL.h>
#include <SDL_opengl.h>
#include <SDL_image.h>
#else
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <SDL2/SDL_image.h>
#endif

#include "../program.h"
#include "../helpers.h"
#include "wall_rotation.h"

// x, y, r, g, b, u, v
GLfloat vertex_data[] = {
     0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
     0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
    -0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f,
    -0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
};

GLuint indices[] = {
    0, 1, 3,
    1, 2, 3
};

WallRotation::WallRotation(SDL_Window* window) : rotation(0.0f), Program(false) {
    // Load texture
    texture = helpers::load_image(window, FILE_TEXTURE, success);
    if (!success) return;

    // Create Vertex Buffer Object (VBO)
    glGenBuffers(1, &VBO);
    // Create Element Buffer Object (EBO)
    glGenBuffers(1, &EBO);
    // Create Vertex Array Object (VAO)
    glGenVertexArrays(1, &VAO);
    
    // Bind VAO
    glBindVertexArray(VAO);
    // Bind VBO to Array buffer
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    // Set VBO data
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertex_data), vertex_data, GL_STATIC_DRAW);

    // Bind EBO
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    // Set vertex attributes pointers
    // X, Y vertex
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 7 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    // R, G, B color
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 7 * sizeof(float), (void*)(2 * sizeof(float)));
    glEnableVertexAttribArray(1);
    // U, V texture
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 7 * sizeof(float), (void*)(5 * sizeof(float)));
    glEnableVertexAttribArray(2);

    // Create shader program
    shader = new Shader(FILE_SHADER);
    if (shader == nullptr || !shader->isSuccess()) return;

    // Get Uniform from shader
    transformId = shader->getUniformLocation("transform");
    if (transformId == -1) return;

    transform = glm::mat4(1.0f);
    std::cout << transform[0][0] << ", " << transform[0][1] << ", " << transform[0][2] << ", " << transform[0][3] << std::endl;
    std::cout << transform[1][0] << ", " << transform[1][1] << ", " << transform[1][2] << ", " << transform[1][3] << std::endl;
    std::cout << transform[2][0] << ", " << transform[2][1] << ", " << transform[2][2] << ", " << transform[2][3] << std::endl;
    std::cout << transform[3][0] << ", " << transform[3][1] << ", " << transform[3][2] << ", " << transform[3][3] << std::endl;

    success = true;
}

WallRotation::~WallRotation() {
    delete shader;
    glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &EBO);
}

void WallRotation::update(Uint64 delta_time) {
    rotation += (M_PI / 2.0f * (float)delta_time / 1000.0f);
    if (rotation > 360) rotation -= 360.0f;
    std::cout << "rotation: " << rotation << std::endl;
}

void WallRotation::draw() {
    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    // Establecemos el shader como programa
    shader->useProgram();

    for (int i = 0; i < 3; i++) {
        // Estable la textura
        glBindTexture(GL_TEXTURE_2D, texture);

        // Establece la matriz de transformacion
        transform = glm::mat4(1.0f);
        // Recordad aplicar siempre T * S * R * v
        transform = glm::translate(transform, glm::vec3(-0.5f + 0.5f * (float)i, 0.25f, 0.0f));
        transform = glm::scale(transform, glm::vec3(0.25, 0.25, 0.25)); 
        transform = glm::rotate(transform, glm::radians(90.0f), glm::vec3(0.0, 0.0, 1.0));
        shader->setUniformMat4(transformId, transform);

        // Dibuja la textura
        glBindVertexArray(VAO);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
    }

    for (int i = 0; i < 2; i++) {
        // Estable la textura
        glBindTexture(GL_TEXTURE_2D, texture);

        // Establece la matriz de transformacion
        transform = glm::mat4(1.0f);
        transform = glm::translate(transform, glm::vec3(-0.3f + 0.6f * (float)i, -0.25f, 0.0f));
        transform = glm::scale(transform, glm::vec3(0.2f + 0.1f * (float)i, 0.2f + 0.1f * (float)i, 0.2f + 0.1f * (float)i)); 
        transform = glm::rotate(transform, rotation * (i % 2 == 0 ? 1.0f : -1.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        shader->setUniformMat4(transformId, transform);

        // Dibuja la textura
        glBindVertexArray(VAO);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
    }

    // Disable VAO
	glBindVertexArray(0);
	// Unbind program
    glUseProgram(0);
}