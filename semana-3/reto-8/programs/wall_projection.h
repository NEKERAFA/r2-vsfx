#include <string>

#ifdef _WIN32
#include <SDL.h>
#include <SDL_opengl.h>
#else
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#endif

#include <glm/glm.hpp>

#include "../shader.h"
#include "../program.h"
#include "../camera.h"

#ifndef WALL_ROTATION_H
#define WALL_ROTATION_H

class WallProjection : public Program {
    private:
        // Name of file shader
        inline static const std::string FILE_SHADER = "wall";
        // Name of file texture
        inline static const std::string FILE_TEXTURE = "wall.jpg";
        // Vertex Buffer Object
        GLuint VBO;
        // Vertex Array Object
        GLuint VAO;
        // Element Buffer Object
        GLuint EBO;
        // Image object
        GLuint texture;

        // Projection Matrix Uniform
        GLuint projectionId;
        // Projection Matrix Matrix
        glm::mat4 projection;
        // View Matrix Uniform
        GLuint viewId;
        // GL Camera object
        Camera* camera;
        // Model Matrix Uniform
        GLuint modelId;
        // Model Matrix Matrix
        glm::mat4 model;

        // GL Shader object
        Shader* shader;

        // Model position
        glm::vec3 model_position;

        // Movement
        float dFront = 0.0f;
        float dRight = 0.0f;

        // Rotation
        float yaw = -90.0f;
        float pitch = 0.0f;
    public:
        // Create a program to show a wall projected in the screen
        WallProjection(SDL_Window* window);
        ~WallProjection();

        // Called when receives a keyboard event
        void keyboardEvent(const SDL_KeyboardEvent& e) override;
        // Called when receives a mouse motion event
        void mouseMotionEvent(const SDL_MouseMotionEvent& e) override;
        // Update program info
        void update(const Uint64 delta_time) override;
        // Draw program
        void draw() override;
};

#endif // WALL_ROTATION_H