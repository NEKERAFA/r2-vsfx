#include <iostream>

#include <glad/gl.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#ifdef _WIN32
#include <SDL.h>
#include <SDL_opengl.h>
#include <SDL_image.h>
#else
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <SDL2/SDL_image.h>
#endif

#include "../config.h"
#include "../program.h"
#include "../helpers.h"
#include "wall_projection.h"

// x, y, z
// r, g, b
// u, v
GLfloat vertex_data[] = {
     0.5f,  0.5f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
     0.5f, -0.5f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
    -0.5f, -0.5f, 0.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f,
    -0.5f,  0.5f, 0.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
};

GLuint indices[] = {
    0, 1, 3,
    1, 2, 3
};

WallProjection::WallProjection(SDL_Window* window) : model_position(glm::vec3(0.0f, 0.0f, 3.0f)), Program(false) {
    // Load texture
    texture = helpers::load_image(window, FILE_TEXTURE, success);
    if (!success) return;

    // Create Vertex Buffer Object (VBO)
    glGenBuffers(1, &VBO);
    // Create Element Buffer Object (EBO)
    glGenBuffers(1, &EBO);
    // Create Vertex Array Object (VAO)
    glGenVertexArrays(1, &VAO);
    
    // Bind VAO
    glBindVertexArray(VAO);
    // Bind VBO to Array buffer
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    // Set VBO data
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertex_data), vertex_data, GL_STATIC_DRAW);

    // Bind EBO
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    // Set vertex attributes pointers
    // X, Y, Z vertex
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    // R, G, B color
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);
    // U, V texture
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
    glEnableVertexAttribArray(2);

    // Create shader program
    shader = new Shader(FILE_SHADER);
    if (shader == nullptr || !shader->isSuccess()) return;

    // Get uniforms from shader
    modelId = shader->getUniformLocation("model");
    viewId = shader->getUniformLocation("view");
    projectionId = shader->getUniformLocation("projection");

    // Create new camera
    camera = new Camera(glm::vec3(0.0f, 0.0f, 3.0f));

    success = true;
}

WallProjection::~WallProjection() {
    delete camera;
    delete shader;
    glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &EBO);
}

// Camera speed
const float cam_speed = 2.0f;

void WallProjection::keyboardEvent(const SDL_KeyboardEvent& e) {
    switch (e.type) {
        case SDL_KEYDOWN:
            switch (e.keysym.sym) {
                case SDLK_w:
                    dFront = cam_speed;
                    return;
                case SDLK_s:
                    dFront = -cam_speed;
                    return;
                case SDLK_a:
                    dRight = -cam_speed;
                    return;
                case SDLK_d:
                    dRight = cam_speed;
                    return;
            }
        case SDL_KEYUP:
            switch (e.keysym.sym) {
                case SDLK_w:
                case SDLK_s:
                    dFront = 0.0f;
                    return;
                case SDLK_a:
                case SDLK_d:
                    dRight = 0.0f;
                    return;
            }
    }
}

const float sensitivity = 0.5f;

void WallProjection::mouseMotionEvent(const SDL_MouseMotionEvent& e) {
    yaw += (float)e.xrel * sensitivity;
    pitch -= (float)e.yrel * sensitivity;

    if (pitch >  89.0f)
        pitch =  89.0f;
    if (pitch < -89.0f)
        pitch =  -89.0f;

    camera->rotation(yaw, pitch);
}

void WallProjection::update(const Uint64 delta_time) {
    //model_position.z -= (float)delta_time / 1000.0f;

    camera->move(dFront * (float)delta_time / 1000.0f, dRight * (float)delta_time / 1000.0f);
}

void WallProjection::draw() {
    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    // Establecemos el shader como programa
    shader->useProgram();

    // Estable la textura
    glBindTexture(GL_TEXTURE_2D, texture);

    // Establece la transformada del modelo (TRS)
    model = glm::mat4(1.0f);
    //model = glm::translate(model, model_position);
    model = glm::rotate(model, glm::radians(-55.0f), glm::vec3(1.0f, 0.0f, 0.0f));
    shader->setUniformMat4(modelId, model);
    // Establece la transformada de la cámara (TRS)
    shader->setUniformMat4(viewId, camera->getTransform());
    // Establece la transformada de la perspectiva
    projection = glm::perspective(glm::radians(45.0f), (float)SCREEN_WIDTH/(float)SCREEN_HEIGHT, 0.1f, 100.0f);
    shader->setUniformMat4(projectionId, projection);

    // Dibuja la textura
    glBindVertexArray(VAO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

    // Disable VAO
	glBindVertexArray(0);
	// Unbind program
    glUseProgram(0);
}