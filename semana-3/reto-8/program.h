#ifdef _WIN32
#include <SDL.h>
#else
#include <SDL2/SDL.h>
#endif

#ifndef PROGRAM_H
#define PROGRAM_H

class Program {
    protected:
        bool success;

    public:
        Program(bool success);
        virtual ~Program() = default;
    
        // Is loaded succesfully
        virtual bool isSuccess();
        // Called when receives a keyboard event
        virtual void keyboardEvent(const SDL_KeyboardEvent& e) = 0;
        // Called when receives a mouse motion event
        virtual void mouseMotionEvent(const SDL_MouseMotionEvent& e) = 0;
        // Update program info
        virtual void update(const Uint64 delta_time) = 0;
        // Draw program
        virtual void draw() = 0;
};

#endif // PROGRAM_H