#ifdef _WIN32
#include <SDL.h>
#include <SDL_opengl.h>
#else
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#endif

// Título de la ventana
constexpr char WINDOW_TITLE[] = "Reto 8 - Proyecciones";

// Frames por segundo
const int FRAMES_PER_SECOND = 60;

// Tamaño de la ventana
const GLuint SCREEN_WIDTH = 640;
const GLuint SCREEN_HEIGHT = 480;

// Directorio con las texturas
constexpr char DIRECTORY_TEXTURE[] = "assets/textures";

// Directorio con los shaders
constexpr char DIRECTORY_SHADERS[] = "assets/shaders";
// Shader buffer size
const int CODE_BUFFER_SIZE = 8192;
